
/**
*@autor Kevin Rebollar
**/

#include <iostream>

using namespace std;

/** MAIN
**/

int main(){
	
	
	//Initialize
	int totalVaron = 0;
	int totalMujer = 0;
	int total = 0;
	float percentVaron = 0;
	float percentMujer = 0;
	
	//Display phrase 1
	cout<<"Digite el numero de estudiantes varones: ";
	cin>>totalVaron;
	
	//Display phrase 2
	cout<<"Digite el numero de estudiantes mujeres: ";
	cin>>totalMujer;
	
	//operador total	
	total = totalVaron+totalMujer;
	
	cout<<"\nEl total de estudiantes es: "<<total;
	
	
	percentVaron = totalVaron / total;
	percentMujer = totalMujer / total;
	
	//Display phrase 3
	cout<<"\n\rEl porcentaje de varones del aula es: "<<percentVaron;
	cout<<"\nEl porcentaje de mujeres del aula es: "<<percentMujer;
	
		
	return 0;
}
