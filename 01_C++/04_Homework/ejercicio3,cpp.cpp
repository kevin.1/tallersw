
/**
*@autor Kevin Rebollar
**/

#include <iostream>

using namespace std;

/**MAIN
**/

int main(){
	
	//Initialize
	int aprobados = 0;
	int desaprobados = 0;
	int notables = 0;
	int sobresalientes = 0;
	int total = 0;
	float percentAprobado = 0;
	
	//Display phrase 1
	cout<<"Digite el numero de aprobados: ";
	cin>>aprobados;
	cout<<"Digite el numero de desaprobados: ";
	cin>>desaprobados;
	cout<<"Digite el numero de alumnos notables: ";
	cin>>notables;
	cout<<"Digite el numero de alumnos sobresalientes: ";
	cin>>sobresalientes;
	
	
	//Operador para el total
	total = aprobados+desaprobados;
	
	cout<<"El total de estudiantes es:"<<total;
	
	//Operador para el porcentaje
	percentAprobado = aprobados/total;
	
	cout<<"el porcentaje de aprobados es:"<<percentAprobado;
	
		
	
	
	return 0;
	
}
