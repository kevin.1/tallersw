
/**
*@autor Kevin Rebollar
**/

#include <iostream>

using namespace std;

/** MAIN
**/

int main(){
	
	//Initialize
	float inputRadio = 0.0;
	float inputHeight = 0.0;
	float resultLateralArea = 0.0;
	float resultVolume = 0.0;
	float valuePi = 3.14;
	
	//Display phrase 1
	cout<<"Digite el radio del cilindro: ";
	cin>>inputRadio;
	
	//Display phrase 2
	cout<<"Digite la altura del cilindro: ";
	cin>>inputHeight;
	
	//operador para el �rea lateral y volumen
	resultLateralArea = 2*inputRadio*inputHeight*valuePi;
	resultVolume = valuePi*inputRadio*inputRadio*inputHeight;
	
	//Display of the result
	cout<<"El area lateral del cilindro es: "<<resultLateralArea;
	cout<<"\nEl volumen del cilindro es: "<<resultVolume;
	
	
	return 0;
}
