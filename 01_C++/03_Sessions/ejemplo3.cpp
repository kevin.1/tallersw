#include <iostream>

using namespace std;

/*
Funcion ejemplo 03
*/

int main(){
	
	//Declaration of example 03
	int numberInput;
	int resultSum;
	int resultRest;
	
	//Initialize
	numberInput= 0;
	resultSum= 0;
	resultRest= 0;
	
	//Display phrase 1
	cout<<"Escriba un numero entero :";
	cin>>numberInput;
	
	//operador suma y resta
	resultSum = numberInput+3;
	resultRest = numberInput-2;
	
	//Display phrase 2 with number
	cout<<"el resultado de la suma es :"<<resultSum;
	cout<<"\n\rel resultado de la resta es :"<<resultRest;
	
	return 0;
	
}
