#include <iostream>

using namespace std;

int main(){
/*
Description de la funcion:

esta es la funcion main de nuestro
programa, es la clase Taller SW
Dirigido por: 
*/	
	//Declaration
	int myFirstVariable;
	int mySecondVariable;
	float myFirstFloat;
	double myFirstDouble;
	char myFirstChar;
	
	//Initialize
	myFirstVariable = 1;
	myFirstFloat = 2.512345678;
	myFirstDouble = 2.599;
	myFirstChar = 'c';
	
	//Display of the variable
	cout<<myFirstChar;
	
	return 0;
}
